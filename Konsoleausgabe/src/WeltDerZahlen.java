/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Nguyen >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8;

    // Anzahl der Sterne in unserer Milchstra�e
     long anzahlSterne = 200000000000l;
    
    // Wie viele Einwohner hat Berlin?
      int bewohnerBerlin = 3660000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
     short  alterTage = 6836;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
     int  gewichtKilogramm =  190000;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
      float flaecheGroessteLand = 17.098242f;
    
    // Wie gro� ist das kleinste Land der Erde?
    
      int flaecheKleinsteLand = 1000000;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    System.out.println("Anzahl Anzahl der Sterne in der Milchstra�e: " + anzahlSterne );
    System.out.println("Anzahl der Einwohner in Berlin: " + bewohnerBerlin);
    System.out.println("Anzahl der Tage meines Alters: " + alterTage);
    System.out.println("Das Schwerste Tier der Welt wiegt: " + gewichtKilogramm );
    System.out.println("Das Gr��te Land der Welt: " + flaecheGroessteLand);
    System.out.println("Das Kleinste Land der Welt: " + flaecheKleinsteLand);
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
