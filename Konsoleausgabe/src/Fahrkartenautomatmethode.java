import java.util.Locale;
import java.util.Scanner;

class Fahrkartenautomatmethode {
	static Scanner tastatur = new Scanner(System.in);

	// Fahrkartenbestellung Erfassen
	public static double fahrkartenbestellung_Erfassen() {
		System.out.print("Zu zahlender Betrag (EURO): ");
		double zuZahlenderBetrag = tastatur.nextDouble();
		return zuZahlenderBetrag;
	}

	public static double anzahl_der_Tickets(double zuZahlenderBetrag) {
		System.out.print("Anzahl der Tickets: ");
		int anzahltickets = tastatur.nextInt();
		zuZahlenderBetrag = anzahltickets * zuZahlenderBetrag;
		return zuZahlenderBetrag;
	}

	// Fahrkarten Bezahlen
	public static double fahrkarten_Bezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf(Locale.US, "Noch zu zahlen: %.2f %s \n",
					(zuZahlenderBetrag / 100 - eingezahlterGesamtbetrag / 100), "Euro");
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneM�nze = tastatur.nextDouble();
			eingeworfeneM�nze = eingeworfeneM�nze * 100;
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return r�ckgabebetrag;
	}

	// Fahrkarten Ausgaben

	public static void fahrkarten_Ausgaben(String fahschein_Text) {

		System.out.println(fahschein_Text);
	}

	// Wartezeit
	public static void warte(int millisekunde) {

		for (int i = 0; i < 8; i++) {
			System.out.print("=");

			try {
				Thread.sleep(millisekunde);
			}

			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	// R�ckgeld Ausgaben
	public static void rueckgeld_Ausgaben(double r�ckgabebetrag) {
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", r�ckgabebetrag / 100);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			int r�ckgabebetrag1 = (int) r�ckgabebetrag;
			r�ckgabebetrag = r�ckgabebetrag1;

			// M�nze Ausgaben
			while (r�ckgabebetrag1 >= 200) {
				muenzeAusgeben(r�ckgabebetrag1, "2 EURO");
				r�ckgabebetrag1 -= 200;
			}
			while (r�ckgabebetrag1 >= 100) {
				muenzeAusgeben(r�ckgabebetrag1, "1 EURO");
				r�ckgabebetrag1 -= 100;
			}
			while (r�ckgabebetrag1 >= 50) {
				muenzeAusgeben(r�ckgabebetrag1, "50 CENT");
				r�ckgabebetrag1 -= 50;
			}
			while (r�ckgabebetrag1 >= 20) {
				muenzeAusgeben(r�ckgabebetrag1, "20 CENT");
				r�ckgabebetrag1 -= 20;
			}
			while (r�ckgabebetrag1 >= 10) {
				muenzeAusgeben(r�ckgabebetrag1, "10 CENT");
				r�ckgabebetrag1 -= 10;
			}
			while (r�ckgabebetrag1 >= 5) {
				muenzeAusgeben(r�ckgabebetrag1, "5 CENT");
				r�ckgabebetrag1 -= 5;
			}
			while (r�ckgabebetrag1 >= 2) {
				muenzeAusgeben(r�ckgabebetrag1, "2 CENT");
				r�ckgabebetrag1 -= 2;
			}
			while (r�ckgabebetrag1 >= 1) {
				muenzeAusgeben(r�ckgabebetrag1, "1 CENT");
				r�ckgabebetrag1 -= 1;
			}
		}

	}

	// M�nze Ausgaben
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(einheit);
	}

	// Benutzer Begr��ung
	public static void begrue�ung() {
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}

	// Main Methode
	public static void main(String[] args) {

		// Fahrkartenbestellung Erfassen
		double zuZahlenderBetrag = fahrkartenbestellung_Erfassen();

		// kommanachstellen
		int zuZahlenderBetrag1 = (int) (zuZahlenderBetrag * 100.1);
		zuZahlenderBetrag = zuZahlenderBetrag1;

		// Anzahl der Tickets
		double anzahl_der_Tickets = anzahl_der_Tickets(zuZahlenderBetrag);

		// Fahrkarten Bezahlen
		double eingezahlterGesamtbetrag = 0.0;
		double r�ckgabebetrag = fahrkarten_Bezahlen(eingezahlterGesamtbetrag, anzahl_der_Tickets);

		// Fahrschein Ausgabe
		fahrkarten_Ausgaben("\nFahrschein wird ausgegeben");

		// Wartezeit
		int millisekunde = 250;
		warte(millisekunde);

		// R�ckgeld Ausgaben
		rueckgeld_Ausgaben(r�ckgabebetrag);
		// Scanner schliessen
		tastatur.close();
		// Benutzer Begr��ung
		begrue�ung();

	}
}