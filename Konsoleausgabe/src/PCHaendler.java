
import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		String artikel;
		int anzahl;
		double preis;
		double mwst;
		double nettogesamtpreis;
		double bruttogesamtpreis;
		
		Scanner myScanner = new Scanner(System.in);
		artikel = liesString("Was möchten sie bestellen? ", myScanner);
		anzahl = liesInt("Geben Sie die Anzahl ein:", myScanner);
		preis = liesDouble("Geben Sie den Nettopreis ein:", myScanner);
		mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:", myScanner);
		nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		
		myScanner.close();
		
	}
		
		public static String liesString(String text, Scanner myScanner) {
			System.out.print(text);
			String artikel = myScanner.next();
			return artikel; 
			
		}
		
		public static int liesInt(String text, Scanner myScanner) {
			System.out.println("Geben Sie die Anzahl ein:");
			int anzahl = myScanner.nextInt(); 
			return anzahl;
		}
		
		public static double liesDouble(String text, Scanner myScanner) {
			System.out.println(text);
			double preis = myScanner.nextDouble();
			return preis;
			
	
		}
		
		public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
			double nettogesamtpreis = anzahl * nettopreis;
			return nettogesamtpreis;
			
			
		}
		
		public static double berechneGesamtbruttopreis(double nettogesamtpreis,
		double mwst) {
			double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
			return bruttogesamtpreis;
			
			
		}
		
		public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst){
			System.out.println("\tRechnung");
			System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
			System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

			
		}
		
	}
