/**
 * 
 */

/**
 * @author Giabao Nguyen
 *
 */
import java.util.Locale;
import java.util.Scanner;

class Fahrkartenautomat {
	static Scanner tastatur = new Scanner(System.in);

	// Fahrkartenbestellung Erfassen
	public static double fahrkartenbestellung_Erfassen() {
		System.out.print("Zu zahlender Betrag (EURO): ");
		double zuZahlenderBetrag = tastatur.nextDouble();
		return zuZahlenderBetrag;
	}

	// Fahrkarten Bezahlen
	public static double fahrkarten_Bezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf(Locale.US, "Noch zu zahlen: %.2f %s \n",
					(zuZahlenderBetrag / 100 - eingezahlterGesamtbetrag / 100), "Euro");
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneM�nze = tastatur.nextDouble();
			eingeworfeneM�nze = eingeworfeneM�nze * 100;
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return r�ckgabebetrag;
	}

	// Fahrkarten Ausgaben

	public static void fahrkarten_Ausgaben(String fahschein_Text) {

		System.out.println(fahschein_Text);
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	// R�ckgeld Ausgaben
	public static void rueckgeld_Ausgaben(double r�ckgabebetrag) {
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", r�ckgabebetrag / 100);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 200) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 200;
			}
			while (r�ckgabebetrag >= 100) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 100;
			}
			while (r�ckgabebetrag >= 50) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 50;
			}
			while (r�ckgabebetrag >= 20) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 20;
			}
			while (r�ckgabebetrag >= 10) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 10;
			}
			while (r�ckgabebetrag >= 5)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 5;
			}
			while (r�ckgabebetrag >= 2)// 2 CENT-M�nzen
			{
				System.out.println("2 CENT");
				r�ckgabebetrag -= 2;
			}
			while (r�ckgabebetrag >= 1)// 1 CENT-M�nzen
			{
				System.out.println("1 CENT");
				r�ckgabebetrag -= 1;
			}

		}

	}

	// Main Methode
	public static void main(String[] args) {

		// Fahrkartenbestellung Erfassen
		double zuZahlenderBetrag = fahrkartenbestellung_Erfassen();

		// kommanachstellen
    	int	zuZahlenderBetrag1 = (int) (zuZahlenderBetrag * 100.1);
		zuZahlenderBetrag = zuZahlenderBetrag1;

		// Anzahl der Tickets
		System.out.print("Anzahl der Tickets: ");
		int anzahltickets = tastatur.nextInt();
		zuZahlenderBetrag = anzahltickets * zuZahlenderBetrag;

		// Fahrkarten Bezahlen
		double eingezahlterGesamtbetrag = 0.0;
		double r�ckgabebetrag = fahrkarten_Bezahlen(eingezahlterGesamtbetrag, zuZahlenderBetrag);

		// Fahrschein Ausgabe
		fahrkarten_Ausgaben("\nFahrschein wird ausgegeben");

		// R�ckgeld Ausgaben
		rueckgeld_Ausgaben(r�ckgabebetrag);

		tastatur.close();

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}
}
